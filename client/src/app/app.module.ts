import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ProviderService } from './provider.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlertComponent } from './alert/alert.component';
import { AlertService } from './alert/alert.service';


import { AppComponent } from './app.component';
import { FreeCupcakeFormComponent } from './free-cupcake-form/free-cupcake-form.component';
import { CollectionComponent } from './collection/collection.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { PrizePopupComponent } from './prize-popup/prize-popup.component';
import { SearchCollectionAddressComponent } from './search-collection-address/search-collection-address.component';
import { CollectionApprovalsComponent } from './collection-approvals/collection-approvals.component';

@NgModule({
  declarations: [
    AppComponent,
    FreeCupcakeFormComponent,
    AlertComponent,
    CollectionComponent,
    MarketplaceComponent,
    PrizePopupComponent,
    SearchCollectionAddressComponent,
    CollectionApprovalsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [
    ProviderService,
    AlertService,
  ],
  bootstrap: [
    AppComponent,
    FreeCupcakeFormComponent,
    AlertComponent,
    CollectionComponent,
    MarketplaceComponent,
    CollectionApprovalsComponent
  ]
})
export class AppModule { }
