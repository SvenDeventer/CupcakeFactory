import { Component, OnInit, NgZone } from '@angular/core';
import { Cupcake } from '../cupcake';
import { ProviderService } from './../provider.service';
import { empty } from 'rxjs/Observer';

import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  cupcakeFactory;
  web3;
  account;
  cupcakesMarket = [];

  constructor(private provider: ProviderService, private _ngZone: NgZone, private alertService: AlertService) { }

  ngOnInit() {
    const app = document.getElementById('marketplace').classList[0];

    if (app !== 'hide') {
      this.web3 = this.provider.setProvider(window['web3']);

      // Get Active account of Metamask or Mist.
      this.provider.getAccount(this.web3).subscribe((accounts) => {
        this._ngZone.run(() => {
          this.account = accounts[0];
        });

        if (this.account !== undefined) {
          this.provider.getContract().subscribe((data) => {
            if (data) {
              this.cupcakeFactory = data;
              this.cupcakeFactory.setProvider(this.web3.currentProvider);

              this.provider.getCupcakesByTradable(this.cupcakeFactory).subscribe((object: any) => {
                if (object.length !== 0) {
                  this.provider
                    .getCupcakesByIds(this.account, this.cupcakeFactory, this.web3, object)
                    .subscribe((cupcakes: any) => {
                      this._ngZone.run(() => {
                        this.cupcakesMarket = cupcakes;
                      });
                    });
                }
              });
            }
          });
        }
      });
    }
  }

  BuyCupcake($event) {
    const id = $event.srcElement.attributes.id.nodeValue;

    this.provider.getOwnerOf(this.cupcakeFactory, id).subscribe(address => {
      console.log(address);
      this.provider.transferFrom(this.account, address, this.cupcakeFactory, this.web3, id, 1, false).subscribe(data => {
        console.log(data);
      });
    });
  }

}
