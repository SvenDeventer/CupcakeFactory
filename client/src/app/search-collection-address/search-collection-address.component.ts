import { Component, OnInit, NgZone } from '@angular/core';
import { ProviderService } from './../provider.service';
import { AlertService } from '../alert/alert.service';


@Component({
  selector: 'app-search-collection-address',
  templateUrl: './search-collection-address.component.html',
  styleUrls: ['./search-collection-address.component.css']
})
export class SearchCollectionAddressComponent implements OnInit {

  address: string;
  web3;
  account;
  cupcakeFactory;
  searchCupcakes = [];

  constructor(private provider: ProviderService, private _ngZone: NgZone, private alertService: AlertService) { }

  ngOnInit() {
    this.web3 = this.provider.setProvider(window['web3']);

    // Get Active account of Metamask or Mist.
    this.provider.getAccount(this.web3).subscribe((accounts) => {
      this._ngZone.run(() => {
        this.account = accounts[0];
      });

      if (this.account !== undefined) {
        this.provider.getContract().subscribe((data) => {
          if (data) {
            this.cupcakeFactory = data;
            this.cupcakeFactory.setProvider(this.web3.currentProvider);
          }
        });
      }
    });
  }

  onSubmit(event) {
    event.preventDefault();
    const bool = this.web3._extend.utils.isAddress(this.address);

    if (bool) {
      window.location.replace('http://' + window.location.hostname + ':3005/collection?adr=' + this.address);
    } else {
      this.alertService.warn('The address (' + this.address + ') giving is not an know address, please check again!');
    }
  }

}
