import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCollectionAddressComponent } from './search-collection-address.component';

describe('SearchCollectionAddressComponent', () => {
  let component: SearchCollectionAddressComponent;
  let fixture: ComponentFixture<SearchCollectionAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchCollectionAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCollectionAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
