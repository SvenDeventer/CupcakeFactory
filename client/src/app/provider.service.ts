import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { default as Web3 } from 'web3';
import { default as contract } from 'truffle-contract';
import { Observable  } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { mergeMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { AlertService } from './alert/alert.service';

import * as cupcake_atrifacts from '../../../build/contracts/CupcakeToken.json';

import { Cupcake } from './cupcake';
import { Approval } from './approval';

@Injectable()
export class ProviderService {

  constructor(private alertService: AlertService) { }

  private HEAD = 30;
  private BODY = 30;
  private url = 'ws://localhost:';

  setProvider(web3) {
    if (web3 !== 'undefined') {
      web3 = new Web3(web3.currentProvider);
      return web3;
    }
    web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    return web3;
  }

  getProvider(web3) {
    let providerName = 'UNKNOWN';

    if (web3.currentProvider.constructor.name === 'MetamaskInpageProvider') {
      providerName = 'Metamask';
    }else if (web3.currentProvider.constructor.name === 'EthereumProvider') {
      providerName = 'Mist';
    } else if (web3.currentProvider.constructor.name === 'o') {
      providerName = 'Parity';
    } else if (web3.currentProvider.host.indexOf('infura') !== -1) {
      providerName = 'Infura';
    } else if (web3.currentProvider.host.indexOf('localhost') !== -1) {
      providerName = 'Localhost';
    }
    return providerName;
  }

  getAccount(provider) {

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      provider.eth.getAccounts(function (err, accs) {
        if (err != null) {
          errorHandler('Cannot find any accounts, make sure your provider is connected.');
        }

        if (accs.length === 0) {
          errorHandler('Couldnt get any accounts! Make sure your Ethereum client is configured correctly.');
        }

        observer.next(accs);
      });
    });
    return observable;
  }

  getBalance(sender, web3) {

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      web3.eth.getBalance(sender, function (err, balance) {
        if (err != null) {
          errorHandler('Cannot find any accounts, make sure your provider is connected.');
        }
        observer.next(web3._extend.utils.fromWei(balance, 'ether'));
      });
    });
    return observable;
  }

  getContract() {
    const observable = new Observable(observer => {
      const c = contract(cupcake_atrifacts);
      observer.next(c);
    });

    return observable;
  }

  getCreatedCupcake(cupcakeFactory, web3) {

    function getImageString(dec, target) {
      let path = '';
      // console.log(dec);
      if (dec <= 30) {
        path = 'assets/images/blue_' + target + '.png';
      } else if (dec > 30 && dec <= 60) {
        path = 'assets/images/choco_' + target + '.png';
      } else {
        path = 'assets/images/lady_' + target + '.png';
      }
      return path;
    }

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      let cc;
      let newCupcakeEvent;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        newCupcakeEvent = cc.NewCupcake();
      }).then( function (data){
        newCupcakeEvent.watch(function (error, result) {
          if (error) {
            errorHandler('There went something wrong with your transaction, try again later.');
          } else {
            console.log(result);
            const dnaDec = web3._extend.utils.toDecimal(result.args.dna);
            const idDec = web3._extend.utils.toDecimal(result.args.cupcakeId);

            const cupcake = new Cupcake(
              result.args.name,
              result.args.cupcakeId,
              idDec,
              result.args.dna,
              dnaDec,
              getImageString(dnaDec.toString().substr(0, 2), 'head'),
              getImageString(dnaDec.toString().substr(2, 2), 'cup'),
              getImageString(dnaDec.toString().substr(4, 2), 'eyes'),
              getImageString(dnaDec.toString().substr(6, 2), 'mouth'),
              result.args.sender
            );
            observer.next(cupcake);
          }
        });
      });
    });

    return observable;
  }

  eventCupcakeToMarket(cupcakeFactory, web3) {

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      let cc;
      let cupcakeToMarket;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        cupcakeToMarket = cc.CupcakeToMarket();
      }).then(function (data) {
        cupcakeToMarket.watch(function (error, result) {
          if (error) {
            errorHandler('There went something wrong with your transaction, try again later.');
          } else {
            observer.next(result);
          }
        });
      });
    });

    return observable;
  }

  eventGetApproved(cupcakeFactory, web3) {

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      let cc;
      let cupcakeToMarket;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        cupcakeToMarket = cc.GetApproved();
      }).then(function (data) {
        cupcakeToMarket.watch(function (error, result) {
          if (error) {
            errorHandler('There went something wrong with your transaction, try again later.');
          } else {
            observer.next(result);
          }
        });
      });
    });

    return observable;
  }

  eventTransfer(cupcakeFactory, web3) {

    const alert = this.alertService;

    function errorHandler(error: any) {
      const n = error.indexOf('.');
      const message = error.substr(0, n + 1);
      alert.error(message);
    }

    const observable = new Observable(observer => {
      let cc;
      let cupcakeToMarket;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        cupcakeToMarket = cc.Transfer();
      }).then(function (data) {
        cupcakeToMarket.watch(function (error, result) {
          if (error) {
            errorHandler('There went something wrong with your transaction, try again later.');
          } else {
            observer.next(result);
          }
        });
      });
    });

    return observable;
  }

  createFreeCupcake(sender, cupcakeFactory, web3, object) {

    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.createRandom(object.name, { from: sender });
      }).then(function (data) {
        observer.next(data);
        }).catch(error => {
          console.log(error);
        });
    });

    return observable;
  }
  getCupcakesIdsByOwner(sender, cupcakeFactory, web3) {

    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.getCupcakesIdsByOwner(sender);
      }).then(function (data) {
        observer.next(data);
        }).catch((e: any) => this.errorHandler(e.message));
    });

    return observable;
  }

  getCupcakeById(sender, cupcakeFactory, web3) {

    function createCupcakeObject(c, web) {

      function getImageString(dec, target) {
        let path = '';
        // console.log(dec);
        if (dec <= 30) {
          path = 'assets/images/blue_' + target + '.png';
        } else if (dec > 30 && dec <= 60) {
          path = 'assets/images/choco_' + target + '.png';
        } else {
          path = 'assets/images/lady_' + target + '.png';
        }
        return path;
      }

      const dnaDec = web._extend.utils.toDecimal(c[2]);
      const idDec = web._extend.utils.toDecimal(c[0]);

      const cc = new Cupcake(
        c[1],
        c[0],
        idDec,
        c[2],
        dnaDec,
        getImageString(dnaDec.toString().substr(0, 2), 'head'),
        getImageString(dnaDec.toString().substr(2, 2), 'cup'),
        getImageString(dnaDec.toString().substr(4, 2), 'eyes'),
        getImageString(dnaDec.toString().substr(6, 2), 'mouth'),
        sender,
        c[3]
      );

      return cc;
    }

    const cupcake = val =>
      new Promise(resolve =>
        setTimeout(() => {
          let cc;
          cupcakeFactory.deployed().then(function (instance) {
            cc = instance;
            return cc.cupcakes(val);
          }).then(function (pl) {
            const c = createCupcakeObject(pl, web3);
            resolve(c);
            }).catch((e: any) => this.errorHandler(e.message));
        }, 3000)
      );

      return cupcake;
  }

  getCupcakesByIds(sender, cupcakeFactory, web3, object) {
    console.log(object);
    const data = of(object).pipe(mergeMap(ids => forkJoin(ids.map(this.getCupcakeById(sender, cupcakeFactory, web3)))));
    return data;
  }

  setCupcakeOnMarket(sender, cupcakeFactory, web3, id) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.putOnMarket(sender, id, { from: sender, gas: 200000});
      }).then(function (data) {
        observer.next(data);
        }).catch((e: any) => this.errorHandler(e.message));
    });

    return observable;
  }

  getCupcakesByTradable(cupcakeFactory) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.getCupcakesByTradable();
      }).then(function (data) {
        observer.next(data);
        }).catch(error => {
          console.log(error);
        });
    });

    return observable;
  }

  transferFrom(sender, from, cupcakeFactory, web3, id, value, bapproval) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.safeTransferFrom(from, sender, id, bapproval, { from: sender, to: from, value: web3.toWei(value, 'ether')});
      }).then(function (data) {
        observer.next(data);
        }).catch(error => {
          console.log(error);
        });
    });

    return observable;
  }

  getOwnerOf(cupcakeFactory, id) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.ownerOf(id);
      }).then(function (data) {
        observer.next(data);
      }).catch((e: any) => this.errorHandler(e.message));
    });

    return observable;
  }

  getApproved(sender, cupcakeFactory, web3, id, value) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.getApproved(id, value, { from: sender });
      }).then(function (data) {
        observer.next(data);
      }).catch(error => {
        console.log(error);
      });
    });

    return observable;
  }

  getApprovalIdsByOwner(sender, cupcakeFactory, web3) {

    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.getApprovalsIdsByOwner(sender, {from: sender});
      }).then(function (data) {
        observer.next(data);
      }).catch(error => {
        console.log(error);
      });
    });

    return observable;
  }

  getApprovalsIdsByTo(sender, cupcakeFactory, web3) {

    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.getApprovalsIdsByTo(sender, {from: sender});
      }).then(function (data) {
        observer.next(data);
      }).catch((e: any) => this.errorHandler(e.message + '.'));
    });

    return observable;
  }

  getApprovalsByIds(sender, cupcakeFactory, web3, object) {
    const data = of(object).pipe(mergeMap(ids => forkJoin(ids.map(this.getApprovalById(sender, cupcakeFactory, web3)))));
    return data;
  }

  getApprovalById(sender, cupcakeFactory, web3) {

    function createCupcakeObject(c, web) {

      function getImageString(dec, target) {
        let path = '';
        // console.log(dec);
        if (dec <= 30) {
          path = 'assets/images/blue_' + target + '.png';
        } else if (dec > 30 && dec <= 60) {
          path = 'assets/images/choco_' + target + '.png';
        } else {
          path = 'assets/images/lady_' + target + '.png';
        }
        return path;
      }

      const dnaDec = web._extend.utils.toDecimal(c[2]);
      const idDec = web._extend.utils.toDecimal(c[0]);

      const cc = new Cupcake(
        c[1],
        c[0],
        idDec,
        c[2],
        dnaDec,
        getImageString(dnaDec.toString().substr(0, 2), 'head'),
        getImageString(dnaDec.toString().substr(2, 2), 'cup'),
        getImageString(dnaDec.toString().substr(4, 2), 'eyes'),
        getImageString(dnaDec.toString().substr(6, 2), 'mouth'),
        sender,
        c[3]
      );

      return cc;
    }

    const approval = val =>
      new Promise(resolve =>
        setTimeout(() => {
          let cc;
          let a;
          cupcakeFactory.deployed().then(function (instance) {
            cc = instance;
            return cc.approvals(val);
          }).then(function (data) {
            a = data;
            return cc.cupcakes(a[1]);
          }).then( function(c) {
            const id = web3._extend.utils.toDecimal(a[0]);
            const tokenId = web3._extend.utils.toDecimal(a[1]);
            const value = web3._extend.utils.toDecimal(a[4]);
            const cupcake = createCupcakeObject(c, web3);
            const appro = new Approval(id, tokenId, a[2], a[3], value, a[5], a[6], a[7], cupcake);
            resolve(appro);
          }).catch((e: any) => this.errorHandler(e.message));
        }, 3000)
      );

      return approval;
  }

  approve(sender, to, tokenId, approve, cupcakeFactory, web3) {
    const observable = new Observable(observer => {
      let cc;
      cupcakeFactory.deployed().then(function (instance) {
        cc = instance;
        return cc.approve(to, tokenId, approve, {from: sender});
      }).then(function (data) {
        observer.next(data);
      }).catch(error => {
        console.log(error);
      });
    });

    return observable;
  }

  errorHandler(error: any): void {
    const n = error.indexOf('.');
    const message = error.substr(0, n + 1);
    this.alertService.error(message);
  }

}
