import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrizePopupComponent } from './prize-popup.component';

describe('PrizePopupComponent', () => {
  let component: PrizePopupComponent;
  let fixture: ComponentFixture<PrizePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrizePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrizePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
