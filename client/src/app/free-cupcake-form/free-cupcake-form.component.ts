import { Component, OnInit, NgZone, AfterContentInit } from '@angular/core';
import { Cupcake } from '../cupcake';
import { ProviderService } from './../provider.service';
import { empty } from 'rxjs/Observer';

import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-free-cupcake-form',
  templateUrl: './free-cupcake-form.component.html',
  styleUrls: ['./free-cupcake-form.component.css']
})
export class FreeCupcakeFormComponent implements OnInit {
  name: string;
  cupcakeFactory;
  web3;
  account;
  cupcake = null;
  loader;

  constructor(private provider: ProviderService, private _ngZone: NgZone, private alertService: AlertService) {}

  ngOnInit() {
    const app = document.getElementById('create').classList[0];
    this.loader = document.getElementById('loader');

    if (app !== 'hide') {
      this.web3 = this.provider.setProvider(window['web3']);

      // Get Active account of Metamask or Mist.
      this.provider.getAccount(this.web3).subscribe((accounts) => {
        this._ngZone.run(() => {
          this.account = accounts[0];
        });

        if (this.account !== undefined) {
          this.provider.getContract().subscribe((data) => {
            if (data) {
              this.cupcakeFactory = data;
              this.cupcakeFactory.setProvider(this.web3.currentProvider);

              this.provider.getCreatedCupcake(this.cupcakeFactory, this.web3).subscribe((object: Cupcake) => {
                if (object.address === this.account) {
                  this.loader.classList.add('hide');
                  this.alertService.success('Succesfully created ' + object.name + '.');
                  this._ngZone.run(() => {
                    this.cupcake = object;
                  });
                }
              });
            }
          });
        }
      });
    }
  }

  onSubmit(event) {
    event.preventDefault();
    this.loader.classList.remove('hide');
    const cupcake = new Cupcake(this.name);
    this.createCupcake(cupcake);
  }

  createCupcake(cupcake) {
    this.provider.createFreeCupcake(this.account, this.cupcakeFactory, this.web3, cupcake).subscribe((object) => {
      console.log(object);
    });
  }

}
