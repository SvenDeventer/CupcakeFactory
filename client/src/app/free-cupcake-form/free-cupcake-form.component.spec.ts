import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeCupcakeFormComponent } from './free-cupcake-form.component';

describe('FreeCupcakeFormComponent', () => {
  let component: FreeCupcakeFormComponent;
  let fixture: ComponentFixture<FreeCupcakeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeCupcakeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeCupcakeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
