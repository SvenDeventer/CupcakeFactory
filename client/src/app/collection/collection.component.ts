import { Component, OnInit, NgZone } from '@angular/core';

import { Cupcake } from '../cupcake';
import { ProviderService } from './../provider.service';

import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.css']
})
export class CollectionComponent implements OnInit {

  cupcakeFactory;
  web3;
  account;
  cupcakes = [];
  dialog;
  title;

  constructor(private provider: ProviderService, private _ngZone: NgZone, private alertService: AlertService) { }

  ngOnInit() {
    const app = document.getElementById('collection').classList[0];

    if (app !== 'hide') {
      this.web3 = this.provider.setProvider(window['web3']);

      // Get Active account of Metamask or Mist.
      this.provider.getAccount(this.web3).subscribe((accounts) => {
        this._ngZone.run(() => {
          this.account = accounts[0];
        });

        if (this.account !== undefined) {
          this.provider.getContract().subscribe((data) => {
            if (data) {
              this.cupcakeFactory = data;
              this.cupcakeFactory.setProvider(this.web3.currentProvider);

              this.provider.eventCupcakeToMarket(this.cupcakeFactory, this.web3).subscribe((c) => {
                console.log('event');
                this._ngZone.run(() => {
                  this.alertService.success(
                    'Your cupcake "' +
                    c['args'].name +
                    '" is put on the trade market, you wont be able to take action with this one!');
                });
              });

              this.provider.eventGetApproved(this.cupcakeFactory, this.web3).subscribe((c) => {
                console.log('event');
                this._ngZone.run(() => {
                  this.alertService.success(
                    'Your approval was succesfully passed along.');
                });
              });

              const remoteOwner = this.getParameterByName('adr');
              let acc = '';
              if (remoteOwner !== null) {
                acc = remoteOwner;

              }else {
                acc = this.account;
              }

              console.log(acc);

              this.provider
                .getCupcakesIdsByOwner(acc, this.cupcakeFactory, this.web3)
                .subscribe((object) => {
                  if (object[0] !== undefined) {
                    this.provider
                      .getCupcakesByIds(this.account, this.cupcakeFactory, this.web3, object)
                      .subscribe((cupcakes: any) => {
                        this._ngZone.run(() => {
                          if (this.account === acc) {
                            this.title = 'Your Collection';
                          } else {
                            this.title = 'Collection of ' + acc;
                            this.alertService.success('Successfully fetched the collection of ' + acc);
                          }
                          this.cupcakes = cupcakes;
                        });
                      });
                  }
                });
            }
          });
        }
      });
    }
  }

  putOnTradeMarket($event) {
    const id = $event.srcElement.attributes.id.nodeValue;
    this.provider.setCupcakeOnMarket(this.account, this.cupcakeFactory, this.web3, id).subscribe((data) => {});
  }

  ApproveTrade($event) {
    const id = $event.srcElement.attributes.id.nodeValue;
    console.log(id);
    this.provider.getApproved(this.account, this.cupcakeFactory, this.web3, id, 1).subscribe(data => {});
  }

  getParameterByName(name) {
    const url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

}
