export class Cupcake {

  constructor(
    public name: string,
    public id?: number,
    public idDecimal?: number,
    public dna?: number,
    public dnaDecimal?: number,
    public imageHead?: string,
    public imageBody?: string,
    public imageEyes?: string,
    public imageMouth?: string,
    public address?: number,
    public tradable?: boolean
  ) { }

}
