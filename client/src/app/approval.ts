import { Cupcake } from './cupcake';

export class Approval {

  constructor(
    public id: number,
    public tokenId: number,
    public to: string,
    public owner: string,
    public value: number,
    public approved: boolean,
    public proccesed: boolean,
    public payed: boolean,
    public cupcake: Cupcake
  ) { }

}
