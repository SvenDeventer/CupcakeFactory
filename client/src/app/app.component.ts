import { Component, OnInit, NgZone } from '@angular/core';
import { ProviderService } from './provider.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'app';
  web3;
  account;
  balance;
  providerName;
  public cupcake;
  cupcakes = [];

  constructor(private provider: ProviderService, private _ngZone: NgZone) { }

  ngOnInit() {
    // Set provider through web3.
    this.web3 =  this.provider.setProvider(window['web3']);
    console.log(this.web3);

    // Get Active account of Metamask or Mist.
    this.provider.getAccount(this.web3).subscribe(accounts => {
      this._ngZone.run(() => {
        this.providerName = this.provider.getProvider(this.web3);
        this.account = accounts[0];
      });
      if (this.account !== undefined) {
        this.provider.getBalance(this.account, this.web3).subscribe(balance => {
          console.log(balance);
          this._ngZone.run(() => {
            this.balance = balance;
          });
        });
      }
    });

    const app = document.getElementById('app');

    for (let i = 1; i <= app.attributes.length - 3; i++) {
      if (app.attributes[i].nodeValue === 'true') {
        document.getElementById(app.attributes[i].nodeName).classList.remove('hide');
      }
    }
  }
}
