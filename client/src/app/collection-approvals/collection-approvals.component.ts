import { Component, OnInit, NgZone } from '@angular/core';

import { ProviderService } from './../provider.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-collection-approvals',
  templateUrl: './collection-approvals.component.html',
  styleUrls: ['./collection-approvals.component.css']
})
export class CollectionApprovalsComponent implements OnInit {

  cupcakeFactory;
  web3;
  account;
  approvals = [];
  requests = [];

  constructor(private provider: ProviderService, private _ngZone: NgZone, private alertService: AlertService) { }

  ngOnInit() {
    const app = document.getElementById('approvals').classList[0];
    console.log('hello');

    if (app !== 'hide') {
      this.web3 = this.provider.setProvider(window['web3']);

      // Get Active account of Metamask or Mist.
      this.provider.getAccount(this.web3).subscribe((accounts) => {
        this._ngZone.run(() => {
          this.account = accounts[0];
        });

        if (this.account !== undefined) {
          console.log('sup');
          this.provider.getContract().subscribe((data) => {
            if (data) {
              this.cupcakeFactory = data;
              this.cupcakeFactory.setProvider(this.web3.currentProvider);
            }
          });

          this.provider.eventTransfer(this.cupcakeFactory, this.web3).subscribe((object: any) => {
            if (object._to === this.account) {
              this._ngZone.run(() => {
                document.getElementById('pay').classList.add('hide');
                this.alertService.success('Succesfully transfered ' + object._tokenId + '.');
                });
            }
          });

          this.provider.getApprovalIdsByOwner(this.account, this.cupcakeFactory, this.web3).subscribe(data => {
            if (data[0] !== undefined) {
              this.provider.getApprovalsByIds(this.account, this.cupcakeFactory, this.web3, data).subscribe((approvals: any) => {
                this._ngZone.run(() => {
                  console.log(approvals);
                  this.approvals = approvals;
                });
              });
            }
          });

          this.provider.getApprovalsIdsByTo(this.account, this.cupcakeFactory, this.web3).subscribe(data => {
            console.log(data);
            if (data[0] !== undefined) {
              this.provider.getApprovalsByIds(this.account, this.cupcakeFactory, this.web3, data).subscribe((requests: any) => {
                this._ngZone.run(() => {
                  console.log(requests);
                  this.requests = requests;
                });
              });
            }
          });
        }
      });
    }
  }

  acceptTrade(approval) {
    this.provider.approve(
      this.account,
      approval.to,
      approval.tokenId,
      true,
      this.cupcakeFactory,
      this.web3).subscribe( data => {
        console.log(data);
    });
  }

  declineTrade(approval) {
    this.provider.approve(
      this.account,
      approval.to,
      approval.tokenId,
      false,
      this.cupcakeFactory,
      this.web3).subscribe(data => {
        console.log(data);
      });
  }

  BuyCupcake(approval) {
    this.provider.transferFrom(
      this.account, approval.owner, this.cupcakeFactory, this.web3, approval.tokenId, approval.value, true).subscribe(data => {
      console.log(data);
    });
  }

}
