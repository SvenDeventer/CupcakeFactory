import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionApprovalsComponent } from './collection-approvals.component';

describe('CollectionApprovalsComponent', () => {
  let component: CollectionApprovalsComponent;
  let fixture: ComponentFixture<CollectionApprovalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionApprovalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionApprovalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
