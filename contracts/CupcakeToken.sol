pragma solidity ^0.4.19;

import "./CupcakeMarket.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Holder.sol";



contract CupcakeToken is CupcakeMarket, ERC721Holder {

    event Transfer(address indexed _from, address indexed _to, uint256 _tokenId);

    event Approved(address indexed _owner, address indexed _approved, uint256 _tokenId);

    event GetApproved(address indexed _owner, uint256 _tokenId);

    event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);

    struct Approval {
        uint id;
        uint tokenId;
        address to;
        address from;
        uint value;
        bool approved;
        bool proccesed;
        bool payed;
    }

    Approval[] public approvals;

    mapping (uint => uint) cupcakesApprovals;
    mapping(address => uint) internal ownerApprovalsCount;

    function balanceOf(address _owner) external view returns (uint256) {
        return ownerCupcakesCount[_owner];
    }

    function ownerOf(uint256 _tokenId) external view returns (address) {
        return cupcakeToOwner[_tokenId];
    }

    function safeTransferFrom(address _from, address _to, uint256 _tokenId, bool approval) external payable {
        require(msg.sender == _to && _from == cupcakeToOwner[_tokenId] && _to != 0x0);

        if (isContract(_to)) {
            require(onERC721Received(_to, _tokenId, "") == bytes4(keccak256("onERC721Received(address,uint256,bool)")));
        }

        ownerCupcakesCount[_to]++;
        ownerCupcakesCount[_from]--;
        cupcakeToOwner[_tokenId] = _to;
        cupcakes[_tokenId].tradable = true;

        _from.transfer(msg.value);
        emit Transfer(_from, _to, _tokenId);

        if (approval) {
            approvals[cupcakesApprovals[_tokenId]].payed = true;
        }
    }

    function transferFrom(address _from, address _to, uint256 _tokenId, bool approval) external payable {
        require(msg.sender == _to && _from == cupcakeToOwner[_tokenId] && _to != 0x0);

        ownerCupcakesCount[_to]++;
        ownerCupcakesCount[_from]--;
        cupcakeToOwner[_tokenId] = _to;
        cupcakes[_tokenId].tradable = true;

        if (approval) {
            approvals[cupcakesApprovals[_tokenId]].payed = true;
        }

        _from.transfer(msg.value);
        emit Transfer(_from, _to, _tokenId);
    }

    function getApproved(uint256 _tokenId, uint _value) public {
        require(cupcakeToOwner[_tokenId] != msg.sender);

        uint id = approvals.length;
        approvals.push(Approval(id, _tokenId, msg.sender, cupcakeToOwner[_tokenId], _value, false, false, false));
        cupcakesApprovals[_tokenId] = id;
        ownerApprovalsCount[cupcakeToOwner[_tokenId]]++;
        emit GetApproved(cupcakeToOwner[_tokenId], _tokenId);
    }

    function approve(address _approved, uint256 _tokenId, bool _approve) public {
        require(msg.sender == cupcakeToOwner[_tokenId]);

        approvals[cupcakesApprovals[_tokenId]].approved = _approve;
        approvals[cupcakesApprovals[_tokenId]].proccesed = true;
        emit Approved(msg.sender, _approved, _tokenId);
    }

    function isContract(address addr) private view returns (bool) {
        uint size;
        assembly { size := extcodesize(addr) }
        return size > 0;
    }

    function getApprovalsIdsByOwner(address _owner) public view returns(uint[]) {
        require(msg.sender == _owner);

        uint[] memory result = new uint[](ownerApprovalsCount[_owner]);
        uint counter = 0;
        for (uint i = 0; i < approvals.length; i++) {
            if (approvals[i].from == _owner) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    function getApprovalsIdsByTo(address _owner) public view returns(uint[]) {
        require(msg.sender == _owner);

        uint size = _getSizeRequestApprovals(msg.sender);
        uint[] memory result = new uint[](size);
        uint counter = 0;
        for (uint i = 0; i < approvals.length; i++) {
            if (approvals[i].to == msg.sender) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    function _getSizeRequestApprovals(address _sender) private view returns(uint) {
        uint counter = 0;
        for (uint i = 0; i < approvals.length; i++) {
            if (approvals[i].to == _sender) {
                counter++;
            }
        }

        return counter;
    }

}
