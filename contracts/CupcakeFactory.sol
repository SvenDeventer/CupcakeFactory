
pragma solidity ^0.4.21;

/**
* Contract for the creation of cupcakes.
*/

contract CupcakeFactory {

    /**
    * Create an event for the creation of a cupcake.
    * An event is a way to communicate with the front-end of your application,
    * to say something happend on the blockchain.
    */
    event NewCupcake(uint cupcakeId, string name, uint dna, bool _tradable, address sender);

    /**
    * The dna length of the cupcake will be 16.
    */
    uint dnaDigits = 16;
    uint dnaModulus = 10 ** dnaDigits;
    
    /**
    * Create Cupcake object.
    */
    struct Cupcake {
        uint id;
        string name;
        uint dna;
        bool tradable;
    }
        
    /**
    * Create local array for the storage of cupcakes,
    * so you don't need to acces the blockchain.
    */
    Cupcake[] public cupcakes;

    /**
    * Create mappings for mapping the right cupcake to the right owner.
    * And create a count mapping, so the owner knows how many cupcakes he owns.
    */
    mapping(uint => address) public cupcakeToOwner;
    mapping(address => uint) internal ownerCupcakesCount;

    /**
    * this function can be called by anyone who owns a eth address.
    */
    function createRandom(string _name) public
    {
        _createCupcake(_name, _generateRandomDna(_name));
    }

    /**
    * Push the created cupcake to the array,
    * and set the owner to the 'msg.sender' -> eth address of the person who access the contract.
    */
    function _createCupcake(string _name, uint _dna) internal
    {
        uint id =  cupcakes.length;
        cupcakes.push(Cupcake(id, _name, _dna, true));
        cupcakeToOwner[id] = msg.sender;
        ownerCupcakesCount[msg.sender]++;
        emit NewCupcake(id, _name, _dna, true, msg.sender);
    }

    /**
    * Temporarily function.
    * Generate a random dna for the cupcake,
    * the modifier view means that the function
    * doesn't change the storage state of a cupcake on the blockchain in any way.
    * keccak256 returns a hash value of a string, random numbers don't really exist on the blockchain.
    */
    function _generateRandomDna(string _str) private view returns(uint) {
        uint rand = uint(keccak256(_str));
        return rand % dnaModulus;
    }

    /**
    * Get the cupcake Ids by the owner address. 
    */
    function getCupcakesIdsByOwner(address _owner) public view returns(uint[]) {
        uint[] memory result = new uint[](ownerCupcakesCount[_owner]);
        uint counter = 0;
        for (uint i = 0; i < cupcakes.length; i++) {
            if (cupcakeToOwner[i] == _owner) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

}
