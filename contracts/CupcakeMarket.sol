pragma solidity ^0.4.21;

import "./CupcakeFactory.sol";


contract CupcakeMarket is CupcakeFactory {

    event CupcakeToMarket(uint _id, string name, uint dna, bool tradable, address sender);

    function putOnMarket(address _owner, uint256 _tokenId) public {
        require(msg.sender == _owner);
        cupcakes[_tokenId].tradable = false;
        emit CupcakeToMarket(cupcakes[_tokenId].id, cupcakes[_tokenId].name, cupcakes[_tokenId].dna, false, _owner);
    }

    function getCupcakesByTradable() external view returns(uint[]) {
        uint size = _getSizeMarketCupcakes();
        uint[] memory result = new uint[](size);
        uint counter = 0;

        for (uint i = 0; i < cupcakes.length; i++) {
            if (cupcakes[i].tradable == false) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    function _getSizeMarketCupcakes() private view returns(uint) {
        uint counter = 0;
        for (uint i = 0; i < cupcakes.length; i++) {
            if (cupcakes[i].tradable == false) {
                counter++;
            }
        }

        return counter;
    }



}
