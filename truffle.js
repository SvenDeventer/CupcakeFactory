module.exports = {
    networks: {
        development: {
            host: "localhost",
            port: 8545,
            network_id: "1527322585487" // Match any network id
        }
    }
};
