# INSTALL GUIDE:

```
npm install -g truffle
npm install -g ganache-cli

// Start local blockchain on your computer. more information on 'https://github.com/trufflesuite/ganache-cli'.
testrpc

// Get network-id of your local blockchain by
curl -X POST --data '{"jsonrpc":"2.0","method":"net_version","params":[]}' http://localhost:8545
```
## Root

Put de network-id in the file truffle.js

```
npm install
truffle compile
truffle migrate
```

## Client

go to client/

```
npm install
ng build
```

## Metamask

Install metamask as an extension in chrome/firefox
- https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn
- https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/

Load your mnemonic from your local node into metamask:

1. Click 'Restore on seed phrase'
2. Paste your seed
3. Yo good to go!